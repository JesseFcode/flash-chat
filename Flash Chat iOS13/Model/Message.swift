//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Jesse Frederick on 2/19/23.
//  Copyright © 2023 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
